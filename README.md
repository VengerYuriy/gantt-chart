Все данные подтягиваются через GET-запрос:

http://82.202.204.94/tmp/test.php

Спецификация:
- технологический стек React, Redux, TS
- все данные в Gantt Chart берутся через API
- стартовый (слева) месяц для вывода данных определяется на основе минимальной даты тасков из API
- дерево тасков поддерживает свертывание/развертывание при нажатии на стрелочку (на всех уровнях)
- справа от названия таска выводится количество вложенных в него элементов
- цвета блоков определяются в зависимости от уровня вложенности (первый - синий, второй - желтый, 3-4 уровни - зеленый, пятый - желтый)
- блоки дат, которые не влезают справа обрезаются через fade-затенение (см. дизайн, справа).

Пример входящего объекта:


const objExample = {
        chart: {
            id: 1,
            title: "Marketing Launch",
            period_end: "2022-09-08",
            period_start: "2022-09-02",
            sub: [{ 
                id: 2,
                title: "Banners for social networks",
                period_end: "2022-09-07",
                period_start: "2022-09-02",
                sub: [{
                    id: 3,
                    title: "Choosing a platform for ads",
                    period_end: "2022-09-06",
                    period_start: "2022-09-02",
                    sub: [{
                        id: 4,
                        title: "Custom issue level #4",
                        period_end: "2022-09-05",
                        period_start: "2022-09-03",
                        sub: [{
                            id: 5,
                            period_end: "2022-09-05",
                            period_start: "2022-09-04",
                            title: "Custom issue level #5",
                        },{
                            id: 6,
                            period_end: "2022-09-05",
                            period_start: "2022-09-05",
                            title: "Custom task"
                        }]
                    }],
                }],
            }],
        },
        period: "02.09.2022-31.12.2022",
        project: "DMS 2.0"
    }
