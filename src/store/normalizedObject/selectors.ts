import { IState } from "./types";

export const selectNormalizedObject = (state: IState) => state.normalizedObjectReducer
export const selectTaskVisible = (state: IState, id: number) => state.normalizedObjectReducer.tasks[id].visible