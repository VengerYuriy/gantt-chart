import { mockData } from "../../mocks/mockObject"
import { getCorrectObject } from "../../services/getCorrectObject"
import getData from "../../services/getData"
import { INormalizedObject } from "../../types/normalizedObject"
import { ISetCorrectTasksAction } from "./types"

export const setVisiblityAction = (id: number, descendantsId: number[], visible: boolean)=> {
    return {
        type: 'normalizedObject/setVisiblity',
        id: id,
        descendantsId: descendantsId,
        payload: visible
    }
}

export const setCorrectTasksAction = (normalizedObject: INormalizedObject) => {
    return {
        type: 'normalizedObject/getTasks',
        data: normalizedObject
    }
}

export const loadTasks: Function = (url: string) => (dispatch: (action: ISetCorrectTasksAction) => Function) => {
    try {
        getData(url)
            .then (responce => getCorrectObject(mockData)) //from december 2022 the API doesn't work
            .then( object => dispatch(setCorrectTasksAction(object)))

    } catch (e) {
        console.log(e)
    }
}
 
