import { INormalizedObject } from "../../types/normalizedObject";

export interface IState {
   normalizedObjectReducer: INormalizedObject
}

export interface INormalizedObjectActions {
    type: string,
    id: number,
    descendantsId: number[],
    data: INormalizedObject,
    payload: boolean,  
}

export interface ISetCorrectTasksAction {
    type: string; 
    data: INormalizedObject;
}