
import { INormalizedObject } from "../../types/normalizedObject";
import { INormalizedObjectActions } from "./types";

const initialState = {
    period_end: '',
    period_start: '',
    project: 'Нет данных',
    datesForChart: new Array(7).fill(new Array(7).fill(new Date())),
    tasks: [{
        id: 0,
        title: 'Нет данных для отображения',
        period_end: '',
        period_start: '',
        nestLevel: 0,
        childrensAmount: 0,
        descendantsId: [],
        descendantsVisible: false,
        visible: false,
    }]
}

const normalizedObjectReducer = (state: INormalizedObject = initialState, action: INormalizedObjectActions ) => {

    switch (action.type) {
        case 'normalizedObject/getTasks': 
            return {...action.data}
        case 'normalizedObject/setVisiblity':
            return {...state, tasks: state.tasks.map( item => {
                if (item.id === action.id) {
                    item.descendantsVisible = action.payload
                    return item
                } else if (action.descendantsId.includes(item.id)) {
                    item.visible = action.payload
                    item.descendantsVisible = action.payload
                    return item
                } else return item
            }) }
        default:
            return state
    }
}

export default normalizedObjectReducer;

