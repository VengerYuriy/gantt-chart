import { applyMiddleware, combineReducers, legacy_createStore as createStore } from "redux";
import thunk from "redux-thunk";
import normalizedObjectReducer from "./normalizedObject/reducer";

const rootReducer = combineReducers({normalizedObjectReducer});

const store = createStore(rootReducer, applyMiddleware(thunk));

export default store