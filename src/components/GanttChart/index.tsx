import { useDispatch } from "react-redux";
import { useEffect } from "react";
import './styles.scss'
import TasksTree from "../WorkTree";
import DateItem from "../DateItem";
import Chart from "../Chart";
import { INormalizedObject } from "../../types/normalizedObject";
import { loadTasks } from "../../store/normalizedObject/actions";

const GanttChart = ({data}: {data: INormalizedObject}) => {

    const title = `${data.project} / ${data.period_start.split('-').reverse().join('.')}-${data.period_end.split('-').reverse().join('.')}`
    let activeRow = 2    
    const dispatch = useDispatch()
    const gridStyles = {
        gridTemplateColumns: `repeat(${data.datesForChart.length*7},21px)`, 
        gridTemplateRows: `repeat(10, 40px)`
    }

    useEffect(() => {
        dispatch(loadTasks('http://82.202.204.94/tmp/test.php'))
    },[dispatch])
    
    return (
        <div className="main-wrapper">
            <div className="main-title">
                <p>{title}</p>
            </div>
            <div className="gantt__wrapper">
                <TasksTree  data={data}/>
                <div className="gantt__charts__wrapper">
                    <div className="gantt__charts__dates">
                        {data.datesForChart.map((item, index) => <DateItem item={item} index={index} key={index}/>)}
                    </div>
                    <div className="gantt__charts__table" style={gridStyles}>
                        {data.datesForChart.flat(Infinity).map((item, index) => <div className='gantt__charts__table__verticalLines' style={{gridColumn: index+1, gridRowStart: 1, gridRowEnd: 20}} key={index}/>)}             
                        {data.tasks.map((item, index) => item.visible && <Chart item={item} data={data} key={index} currentRow={activeRow++}/>)}
                    </div>
                </div>
            </div>
        </div>
    )
}

export default GanttChart;