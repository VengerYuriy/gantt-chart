import { INormalizedObject, INormalizedObjectItem } from "../../types/normalizedObject";
import './styles.scss'


const Chart = ({item, data, currentRow}: {item: INormalizedObjectItem, data: INormalizedObject, currentRow: number} ) => {
    const firstCell = data.datesForChart[0][0];
    const startColumn = ((+new Date(item.period_start) - +firstCell)/(1000*60*60*24) + 1);
    const durationTask = (+new Date(item.period_end) - +new Date(item.period_start))/(1000*60*60*24) + 1;

    const objPosition = {
        gridColumnStart: startColumn, 
        gridColumnEnd: startColumn + durationTask, 
        gridRow: currentRow,
    }
        return <div style={objPosition} className={`diagram nest-level-${item.nestLevel}`}/>
}

export default Chart