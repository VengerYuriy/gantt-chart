import './styles.scss'
import arrow from '../../assets/icons/arrow.svg'
import { INormalizedObjectItem } from '../../types/normalizedObject'
import { useDispatch } from 'react-redux'
import { setVisiblityAction } from '../../store/normalizedObject/actions'
import getIcon from '../../services/getIcon'


const TaskItem = ({item}: {item: INormalizedObjectItem}) => {
    const dispatch = useDispatch();
    const descendantsVisible = item.descendantsVisible ? 'show' : 'hide';

    return (
        <div className="work-tree__item" style={{paddingLeft: `${20+20*item.nestLevel}px`}}>
            {<img src={arrow} style={!item.childrensAmount ? {visibility: 'hidden'} : {}} alt='arrow' className={`work-tree__item__arrow ${descendantsVisible}`} onClick={()=> dispatch(setVisiblityAction(item.id, item.descendantsId, !item.descendantsVisible))}/>}
            <img src={getIcon(item.nestLevel)} alt = 'icon' className='work-tree__item__icon'/>
            <h3 className="work-tree__item__descendans">{item.childrensAmount}</h3>
            <h2 className="work-tree__item__title">{item.title}</h2>
        </div>
    )
}

export default TaskItem