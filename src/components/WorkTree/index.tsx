import './styles.scss'
import { INormalizedObject } from "../../types/normalizedObject"
import TaskItem from "../TaskItem"

const TasksTree = ({data}: {data: INormalizedObject}) => {
    
    return (
        <div className="work-tree">
            <div className="work-tree__title">
                <p>Work item</p>
            </div>
            <div className="work-tree__items">
                <div className="work-tree__item"></div>
                {data.tasks.map((item, index)=> 
                    item.visible && <TaskItem item = {item} key={index}/>
                )}
            </div>
        </div>
    )
}

export default TasksTree;