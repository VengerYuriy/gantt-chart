import moment from "moment"
import './styles.scss'

interface IDateItem {
    item: Date [],
    index: number
}

const DateItem = ({item, index}: IDateItem): JSX.Element => {

    return (
        <div className="dates__week">
            <div className="dates__week__title">
                <p>{moment(item[0]).format('DD MMM')} - {moment(item[6]).format('DD MMM')}</p>
            </div>
            <div className="dates__week__items" style={{}}>
                {item.map((item2, index2) => (
                    <div className="dates__week__item" key={index2}>
                        <p data-day = {index} key={index}>{item2.getDate()}</p>
                    </div>
                ))}
            </div>
        </div>
    )
}

export default DateItem;