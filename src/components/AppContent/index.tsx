import { useSelector } from "react-redux";
import { selectNormalizedObject } from "../../store/normalizedObject/selectors";
import GanttChart from "../GanttChart";

const AppContent = () => {

    return (
        <div className="app-content">
            <GanttChart data = {useSelector(selectNormalizedObject)}/>
        </div>
    )
}

export default AppContent;