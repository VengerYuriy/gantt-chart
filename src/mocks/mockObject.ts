export const mockData = {
    chart: {
        id: 1,
        title: "Marketing Launch",
        period_end: "2022-09-08",
        period_start: "2022-09-02",
        sub: [{
            id: 2,
            title: "Banners for social networks",
            period_end: "2022-09-07",
            period_start: "2022-09-02",
            sub: [{
                id: 3,
                title: "Choosing a platform for ads",
                period_end: "2022-09-06",
                period_start: "2022-09-02",
                sub: [{
                    id: 4,
                    title: "Custom issue level #4",
                    period_end: "2022-09-05",
                    period_start: "2022-09-03",
                    sub: [{
                        id: 5,
                        period_end: "2022-09-05",
                        period_start: "2022-09-04",
                        title: "Custom issue level #5",
                        },{
                        id: 6,
                        period_end: "2022-09-05",
                        period_start: "2022-09-05",
                        title: "Custom task"
                        }]
                }],
            }],
        }],
    },
    period: "02.09.2022-31.12.2022",
    project: "DMS 2.0"
}