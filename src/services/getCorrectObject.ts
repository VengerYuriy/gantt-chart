import moment from "moment"
import { IIncomeObject, IIncomeObjectItem } from "../types/incomeObject"
import { INormalizedObjectItem } from "../types/normalizedObject"

export const getCorrectObject = (obj: IIncomeObject) => {

    const getDescendantsId = (object: IIncomeObjectItem, amount: number[] = []) => {
        if (object.sub) {
            object.sub.forEach(item => {
                amount.push(item.id)
                getDescendantsId(item, amount)
            })
        } 
        return amount.flat(Infinity)
    }

    const getNormalaizedTasks = (object: IIncomeObjectItem, parent = -1 , nestLevelParam = 0, normalaizedTasks: INormalizedObjectItem[] = []) => {
        
        const normalaizedObject = {
            id: object.id,
            title: object.title, 
            period_start: object.period_start,
            period_end: object.period_end, 
            nestLevel: nestLevelParam,
            parentId: parent,
            descendantsId: getDescendantsId(object),
            descendantsVisible: true,
            childrensAmount: object.sub ? object.sub.length : 0,
            visible: true
        }
        normalaizedTasks.push(normalaizedObject)

        if (object.sub) {
            nestLevelParam++
            object.sub.forEach((item) => getNormalaizedTasks(item, normalaizedObject.id, nestLevelParam, normalaizedTasks)) 
        }

        return normalaizedTasks
    }

    const getArrayOfDates = (period_start: string, period_end: string) => {
        const startDate = new Date(period_start);
        const endDate = new Date(period_end);
        const fromStartToMonday = +moment(startDate).format('E') -1 ;
        const fromEndToSunday = 7 - +moment(endDate).format('E');
    
        startDate.setDate(startDate.getDate() - fromStartToMonday)
        endDate.setDate(endDate.getDate()+fromEndToSunday)
    
        const differencebetweenDate = (+endDate - +startDate)/(1000*60*60*24)
    
        const arrayOfDate = [];
    
        for (let i = 0, arrayElem = []; i <= differencebetweenDate; i++) {
            const tempDate = new Date (startDate);
            tempDate.setDate(startDate.getDate()+i);
            arrayElem.push(tempDate);
            if (arrayElem.length === 7) {
                arrayOfDate.push(arrayElem);
                arrayElem = [];
            }
        }
        return arrayOfDate;
    }

    const startDate = obj.period.split('-')[0].split('.').reverse().join('-')
    const endDate = obj.period.split('-')[1].split('.').reverse().join('-')

    return {
        project: obj.project,
        period_start: startDate,
        period_end: endDate,
        datesForChart: getArrayOfDates(startDate, endDate),
        tasks: getNormalaizedTasks(obj.chart)
        }
}
