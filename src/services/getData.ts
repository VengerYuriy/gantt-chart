import axios from "axios";

const getData = async (url: string) => (await axios.get(url)).data

export default getData;