import icon_0 from '../assets/icons/icon-0.svg'
import icon_1 from '../assets/icons/icon-1.svg'
import icon_2 from '../assets/icons/icon-2.svg'
import icon_3 from '../assets/icons/icon-3.svg'
import icon_4 from '../assets/icons/icon-4.svg'

const getIcon = (nestLevel: number) => {
    const iconArr = [icon_0, icon_1, icon_2, icon_3, icon_4];
    return iconArr[nestLevel];
}


export default getIcon;