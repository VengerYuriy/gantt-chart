
export interface IIncomeObjectItem {
    id: number,
    title: string,
    period_end: string,
    period_start: string,
    sub?: IIncomeObjectItem[],
}

export interface IIncomeObject {
    chart: IIncomeObjectItem,
    period: string,
    project: string
}