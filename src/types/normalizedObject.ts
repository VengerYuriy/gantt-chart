export interface INormalizedObjectItem {
    id: number,
    title: string,
    period_end: string,
    period_start: string,
    nestLevel: number,
    childrensAmount: number,
    descendantsId: number[],
    descendantsVisible: boolean,
    visible: boolean,
}

export interface INormalizedObject {
    period_end: string
    period_start: string
    project: string
    datesForChart: Date [][],
    tasks: INormalizedObjectItem[]
}